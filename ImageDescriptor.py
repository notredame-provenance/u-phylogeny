import numpy, cv2

def surfDescribe(image, kpCount = 2000, hessian = 100.0, mask = [[]]):
    # obtains the gray-scaled version of the given image
    gsImage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
    # creates a mask to ignore eventual black borders
    _, bMask = cv2.threshold(cv2.normalize(gsImage, alpha = 0, beta = 255, norm_type = cv2.cv.CV_MINMAX),
                             1, 255, cv2.THRESH_BINARY)
    bMask = cv2.convertScaleAbs(bMask)
    
    # combines the border mask to an eventual given mask
    if mask != [[]]:
        mask = cv2.bitwise_and(mask, bMask)
    else:
        mask = bMask
    
    # detects the SURF keypoints
    surfDetectorDescriptor = cv2.SURF(hessian)
    keypoints = surfDetectorDescriptor.detect(gsImage, mask)
    
    # describes the obtained keypoints
    descriptions = []
    kpImage = image
    
    if len(keypoints) > 0:
        # removes the weakest keypoints (according to hessian)
        keypoints = sorted(keypoints, key = lambda match: match.response, reverse = True)
        del keypoints[kpCount:]
        
        # describes the selected keypoints, and draws them 
        keypoints, descriptions = surfDetectorDescriptor.compute(gsImage, keypoints)
        kpImage = cv2.drawKeypoints(image, keypoints, flags = cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    
    # returns keypoints, descriptions, and the image with keypoints
    return keypoints, descriptions, kpImage

def siftDescribe(image, kpCount = 2000, mask = [[]]):
    # obtains the gray-scaled version of the given image
    gsImage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
    # creates a mask to ignore eventual black borders
    _, bMask = cv2.threshold(cv2.normalize(gsImage, alpha = 0, beta = 255, norm_type = cv2.cv.CV_MINMAX),
                             1, 255, cv2.THRESH_BINARY)
    bMask = cv2.convertScaleAbs(bMask)
    
    # combines the border mask to an eventual given mask
    if mask != [[]]:
        mask = cv2.bitwise_and(mask, bMask)
    else:
        mask = bMask
    
    # detects the SIFT keypoints
    siftDetectorDescriptor = cv2.SIFT(kpCount)
    keypoints = siftDetectorDescriptor.detect(gsImage, mask)
    
    # describes and draws the selected keypoints
    descriptions = []
    kpImage = image
    
    if len(keypoints) > 0:
        keypoints, descriptions = siftDetectorDescriptor.compute(gsImage, keypoints)
        kpImage = cv2.drawKeypoints(image, keypoints, flags = cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    
    # returns keypoints, descriptions, and the image with keypoints
    return keypoints, descriptions, kpImage
