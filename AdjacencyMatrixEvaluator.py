import numpy, csv

def loadNPZMatrices(npzFilePath):
    npzFile = numpy.load(npzFilePath)
    kpCountSpanTree  = npzFile['arr_2']
    avgDistSpanTree  = npzFile['arr_3']
    npzFile.close()
    
    return kpCountSpanTree, avgDistSpanTree

def loadCSVMatrix(csvFilePath):
    labels = []
    mat = [[]]
    
    csvFile = open(csvFilePath, "r")
    csvReader = csv.reader(csvFile, delimiter=',')
    
    i = -1
    for row in csvReader:
        if row[-1] == "":
            del row[len(row) - 1:]
        
        i = i + 1
        sz = len(row)
        
        if i == 0:
            for j in range(0, sz):
                labels.append(row[j].strip())
                
            mat = numpy.zeros((sz, sz))
            
        else:
            for j in range(0, sz):
                mat[i-1][j] = row[j]
    
    csvFile.close()
    
    return labels, mat

def evaluateAdjMatrix(usrLabels, usrAdjMatrix, gtLabels, gtAdjMatrix):
    missingNodes = []
    falseNodes = []
    missingEdges = []
    falseEdges = []
    hitNodes = []
    hitEdges = []
    
    usrSize = len(usrLabels)
    gtSize = len(gtLabels)
    
    nodeFN = 0.0
    nodeTP = 0.0
    nodeFP = 0.0
    
    edgeTN = 0.0
    edgeFN = 0.0
    edgeTP = 0.0
    edgeFP = 0.0
    
    # makes the gt matrix simetric
    for i in range(0, gtSize):
        for j in range(i + 1, gtSize):
            if gtAdjMatrix[i][j] == 1 or gtAdjMatrix[j][i] == 1:
                gtAdjMatrix[i][j] = 1
                gtAdjMatrix[j][i] = 1
    
    # compares the usr to the gt matrix
    for i in range(0, gtSize):
        nodeHit = 0
        
        for j in range(0, usrSize):
            if usrLabels[j].find(gtLabels[i]) > -1:
                nodeHit = 1
                hitNodes.append(usrLabels[j])
                
                for k in range(i + 1, gtSize):
                    for l in range(0, usrSize):
                        if usrLabels[l].find(gtLabels[k]) > -1:
                            if gtAdjMatrix[i][k] == 0:
                                if usrAdjMatrix[j][l] == 1:
                                    edgeFP = edgeFP + 1.0
                                    falseEdges.append(usrLabels[j] + " --- " + usrLabels[l])
                                else:
                                    edgeTN = edgeTN + 1.0
                            
                            else:
                                if usrAdjMatrix[j][l] == 1:
                                    edgeTP = edgeTP + 1.0
                                    hitEdges.append(usrLabels[j] + " --- " + usrLabels[l])
                                else:
                                    edgeFN = edgeFN + 1.0
                                    missingEdges.append(usrLabels[j] + " --- " + usrLabels[l])
                                    
                            break
                                    
                break
    
        if nodeHit == 1:
            nodeTP = nodeTP + 1.0
        else:
            nodeFN = nodeFN + 1.0
            missingNodes.append(gtLabels[i])
            
            for j in range(i + 1, gtSize):
                if gtAdjMatrix[i][j] == 1:
                    edgeFN = edgeFN + 1.0
                    missingEdges.append(gtLabels[i] + " --- " + gtLabels[j])
    
    # compares the gt to the usr matrix (treats false positive nodes)
    for i in range(0, usrSize):
        nodeHit = 0
        
        for gtLabel in gtLabels:
            if usrLabels[i].find(gtLabel) > -1:
                nodeHit = 1
                break
        
        if nodeHit == 0:
            for j in range(i + 1, usrSize):
                if usrAdjMatrix[i][j] == 1:
                    nodeFP = nodeFP + 1.0
                    falseNodes.append(usrLabels[i])
                    break
                
            for j in range(i + 1, usrSize):
                if usrAdjMatrix[i][j] == 1:
                    edgeFP = edgeFP + 1.0
                    falseEdges.append(usrLabels[i] + " --- " + usrLabels[j])
    
    NR = 1.0
    if nodeTP + nodeFN > 0.0:
        NR = nodeTP / (nodeTP + nodeFN)
        
    NP = 0.0
    if nodeTP + nodeFP > 0.0:
        NP = nodeTP / (nodeTP + nodeFP)
    
    print "   node recall:", NR
    print "node precision:", NP
    
    ER = 1.0
    if edgeTP + edgeFN > 0.0:
        ER = edgeTP / (edgeTP + edgeFN)
        
    EP = 0.0
    if edgeTP + edgeFP > 0.0:
        EP = edgeTP / (edgeTP + edgeFP)
    
    print "   edge recall:", ER
    print "edge precision:", EP
    
    OM = 2 * (nodeTP + edgeTP) / (nodeTP + nodeFN + nodeTP + nodeFP + edgeTP + edgeFN + edgeTP + edgeFP)
    print " total overlap:", OM
    
    print " Missing nodes:", missingNodes
    print "   False nodes:", falseNodes
    print " Missing edges:", missingEdges
    print "   False edges:", falseEdges
    print "     Hit nodes:", hitNodes
    print "     Hit edges:", hitEdges
    
    print "   Total nodes:", nodeTP + nodeFN
    print "   Total edges:", edgeTP + edgeFN

    return NR, NP, ER, EP, OM

def evaluate(imageListFilePath, npzListFilePath, csvListFilePath):
    imageListFilePaths = []
    npzFilePaths = []
    csvFilePaths = []
    
    file = open(imageListFilePath, 'r')
    for filePath in file:
        imageListFilePaths.append(filePath.strip())
    file.close()
    
    file = open(npzListFilePath, 'r')
    for filePath in file:
        npzFilePaths.append(filePath.strip())
    file.close()
    
    file = open(csvListFilePath, 'r')
    for filePath in file:
        csvFilePaths.append(filePath.strip())
    file.close()
    
    kpCountNRs = []
    kpCountNPs = []
    kpCountERs = []
    kpCountEPs = []
    kpCountOMs = []
    
    avgDistNRs = []
    avgDistNPs = []
    avgDistERs = []
    avgDistEPs = []
    avgDistOMs = []
    
    # for each graph to be evaluated
    for i in range(len(imageListFilePaths)):
        print imageListFilePaths[i]
        
        # current user labels
        usrLabels = []
        file = open(imageListFilePaths[i], 'r')
        for line in file:
            usrLabels.append(line.strip().split("/")[-1])
        file.close()
        
        # current user graphs (keypoint and distance)
        usrKpCountAdjMatrix, usrAvgDistAdjMatrix = loadNPZMatrices(npzFilePaths[i])
        
        # current gt graph
        gtLabels, gtAdjMatrix = loadCSVMatrix(csvFilePaths[i])
        
        NR, NP, ER, EP, OM = evaluateAdjMatrix(usrLabels, usrKpCountAdjMatrix, gtLabels, gtAdjMatrix)
        kpCountNRs.append(NR)
        kpCountNPs.append(NP)
        kpCountERs.append(ER)
        kpCountEPs.append(EP)
        kpCountOMs.append(OM)
        
        NR, NP, ER, EP, OM = evaluateAdjMatrix(usrLabels, usrAvgDistAdjMatrix, gtLabels, gtAdjMatrix)
        avgDistNRs.append(NR)
        avgDistNPs.append(NP)
        avgDistERs.append(ER)
        avgDistEPs.append(EP)
        avgDistOMs.append(OM)
        
        print ""
    
    # calculates averages and standard deviations
    print "******* Results: *******"
    
    NR = numpy.average(kpCountNRs), numpy.std(kpCountNRs)
    NP = numpy.average(kpCountNPs), numpy.std(kpCountNPs)
    ER = numpy.average(kpCountERs), numpy.std(kpCountERs)
    EP = numpy.average(kpCountEPs), numpy.std(kpCountEPs)
    OM = numpy.average(kpCountOMs), numpy.std(kpCountOMs)
    print "   Node Recall based on keypoints:", NR[0], NR[1]
    print "Node Precision based on keypoints:", NP[0], NP[1]
    print "   Edge Recall based on keypoints:", ER[0], ER[1]
    print "Edge Precision based on keypoints:", EP[0], EP[1]
    print " Graph Overlap based on keypoints:", OM[0], OM[1]
    
    NR = numpy.average(avgDistNRs), numpy.std(avgDistNRs)
    NP = numpy.average(avgDistNPs), numpy.std(avgDistNPs)
    ER = numpy.average(avgDistERs), numpy.std(avgDistERs)
    EP = numpy.average(avgDistEPs), numpy.std(avgDistEPs)
    OM = numpy.average(avgDistOMs), numpy.std(avgDistOMs)
    print "   Node Recall based on avrg dist:", NR[0], NR[1]
    print "Node Precision based on avrg dist:", NP[0], NP[1]
    print "   Edge Recall based on avrg dist:", ER[0], ER[1]
    print "Edge Precision based on avrg dist:", EP[0], EP[1]
    print " Graph Overlap based on avrg dist:", OM[0], OM[1]