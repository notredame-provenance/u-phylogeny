# builds two graphs based on (i) mutual information and (ii) minimum squared error
import numpy, cv2, ImageDescriptor, KeypointMatcher, LocalSimilarityAnalyser, ProvenanceGraphBuilder

def buildGraph(imageListFilePath, outputFilePath):
    imageFilePaths = []
    descriptionFilePaths = []    
    
    file = open(imageListFilePath, 'r')
    for filePath in file:
        imageFilePaths.append(filePath.strip())
    file.close()
    
    # adjancency matrices
    imageCount = len(imageFilePaths)
    mifAdjMatrix = numpy.zeros((imageCount, imageCount))
    mseAdjMatrix = numpy.zeros((imageCount, imageCount)) * -1.0
    
    # image descriptions and keypoints
    keypoints = []
    descriptions = []
    for imageFilePath in imageFilePaths:
        img = cv2.imread(imageFilePath)
        kps = []
        descs = []
        
        if img is not None:
            kps, descs, _ = ImageDescriptor.surfDescribe(img)
            
        keypoints.append(kps)
        descriptions.append(descs)
        
    # for each pair of images
    for i in range(imageCount):
        imageI = cv2.imread(imageFilePaths[i])
        if imageI is not None:
            for j in range(i + 1, imageCount):
                print "Processing", i + 1, j + 1, imageCount
                
                imageJ = cv2.imread(imageFilePaths[j])
                if imageJ is not None:
                    wkpsI, warpI, wkpsJ, warpJ, matches, _, _ = KeypointMatcher.match(keypoints[i], descriptions[i], imageI,
                                                                                      keypoints[j], descriptions[j], imageJ)
                    
                    filteredKeypointsI = []
                    filteredKeypointsJ = []
                    filteredWarpKeypsI = []
                    filteredWarpKeypsJ = []
                    for match in matches:
                        filteredKeypointsI.append(keypoints[i][match.queryIdx])
                        filteredKeypointsJ.append(keypoints[j][match.trainIdx])
                        filteredWarpKeypsI.append(wkpsI[match.queryIdx])
                        filteredWarpKeypsJ.append(wkpsJ[match.trainIdx])
                    
                    mifIJ, mseIJ, _ = LocalSimilarityAnalyser.getSimilarity(warpI, filteredWarpKeypsI, imageJ, filteredKeypointsJ)
                    mifJI, mseJI, _ = LocalSimilarityAnalyser.getSimilarity(imageI, filteredKeypointsI, warpJ, filteredWarpKeypsJ)
                    
                    mifAdjMatrix[i, j] = mifIJ
                    mifAdjMatrix[j, i] = mifJI
                    
                    mseAdjMatrix[i, j] = mseIJ
                    mseAdjMatrix[j, i] = mseJI
    
    # applies Kruskal on both matrices
    mifTree = ProvenanceGraphBuilder.applyKruskal(mifAdjMatrix, maxST = True)
    mseTree = ProvenanceGraphBuilder.applyKruskal(mseAdjMatrix, maxST = False)
    numpy.savez(outputFilePath, mifAdjMatrix, mseAdjMatrix, mifTree, mseTree)
