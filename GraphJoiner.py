import numpy, ProvenanceGraphBuilder

def minMaxNorm(adjMatrix, inverse = False):
    if inverse:
        adjMatrix = adjMatrix * -1.0

    values = []
    mSize = adjMatrix.shape[0]
    for i in range(mSize - 1):
        for j in range(i + 1, mSize):
            values.append(adjMatrix[i][j])

    min = numpy.min(values)
    max = numpy.max(values)
    itv = max - min

    for i in range(mSize):
        for j in range(mSize):
            adjMatrix[i][j] = (adjMatrix[i][j] - min) / itv

    return adjMatrix

def join(kpCountAdjMatrix, avgDistAdjMatrix):
    normKpCountAdjMatrix = minMaxNorm(kpCountAdjMatrix)
    normAvgDistAdjMatrix = minMaxNorm(avgDistAdjMatrix, inverse = True)

    kpWeight = 1.0
    distWeight = 1.0

    adjMatrix = (kpWeight * normKpCountAdjMatrix + distWeight * normAvgDistAdjMatrix) / (kpWeight + distWeight)

    return adjMatrix

def buildJointGraph(npzFilePath):
    npzFile = numpy.load(npzFilePath)
    kpCountAdjMatrix = npzFile['arr_0']
    avgDistAdjMatrix = npzFile['arr_1']
    npzFile.close()

    newMatrix = join(kpCountAdjMatrix, avgDistAdjMatrix)
    newMatrixTree = ProvenanceGraphBuilder.applyKruskal(newMatrix, maxST = True)
    numpy.savez(npzFilePath + ".join.npz", newMatrix, newMatrix, newMatrixTree, newMatrixTree)