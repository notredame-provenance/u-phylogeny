import numpy, cv2, math, multiprocessing

# multiprocess shared resource
_filteredMatches = multiprocessing.Manager().list()

def _copyKeypoints(keypoints):
    answer = []
    
    for keypoint in keypoints:
        answer.append(cv2.KeyPoint(keypoint.pt[0], keypoint.pt[1], keypoint.size, keypoint.angle, keypoint.response))
        
    return answer
    

def _filterIthMatch(queryImgKeypoints, trainImgKeypoints, matches, i,
                    refMatchCount, displMatchThreshold):
    global _filteredMatches
    
    ithMatches = []
    usedMatches = []
    for j in range(i + 1, refMatchCount):
        if j not in usedMatches:
            queryImgKeypointCount = len(queryImgKeypoints)
            trainImgKeypointCount = len(trainImgKeypoints)
    
            queryPoints = numpy.zeros((2, queryImgKeypointCount)) 
            for k in range(queryImgKeypointCount):
                queryPoints[0, k] = queryImgKeypoints[k].pt[0]
                queryPoints[1, k] = queryImgKeypoints[k].pt[1]
            
            trainPoints = numpy.zeros((2, trainImgKeypointCount)) 
            for k in range(trainImgKeypointCount):
                trainPoints[0, k] = trainImgKeypoints[k].pt[0]
                trainPoints[1, k] = trainImgKeypoints[k].pt[1]
                
            queryKeypointIndex1 = matches[i].queryIdx
            queryKeypointIndex2 = matches[j].queryIdx
            queryDistance = math.sqrt(pow(queryPoints[0, queryKeypointIndex1] - queryPoints[0, queryKeypointIndex2], 2) +
                                      pow(queryPoints[1, queryKeypointIndex1] - queryPoints[1, queryKeypointIndex2], 2))
            
            trainKeypointIndex1 = matches[i].trainIdx
            trainKeypointIndex2 = matches[j].trainIdx
            trainDistance = math.sqrt(pow(trainPoints[0, trainKeypointIndex1] - trainPoints[0, trainKeypointIndex2], 2) +
                                      pow(trainPoints[1, trainKeypointIndex1] - trainPoints[1, trainKeypointIndex2], 2))
            
            # puts query and train images in the same scale
            distanceRate = 1.0
            if trainDistance > 0:
                distanceRate = queryDistance / trainDistance
            
            if distanceRate > 0:   
                if distanceRate > 1.0:
                    scaleMatrix = numpy.zeros((2, 2))
                    scaleMatrix[0, 0] = distanceRate
                    scaleMatrix[1, 1] = distanceRate
                    trainPoints = numpy.mat(scaleMatrix) * numpy.mat(trainPoints)

                elif distanceRate < 1.0:
                    scaleMatrix = numpy.zeros((2, 2))
                    scaleMatrix[0, 0] = 1.0 / distanceRate
                    scaleMatrix[1, 1] = 1.0 / distanceRate
                    queryPoints = numpy.mat(scaleMatrix) * numpy.mat(queryPoints)
            
                # calculates the difference of angles between query and train images
                # angle between points 1 and 2, within query image
                queryAngle = math.atan2(queryPoints[1, queryKeypointIndex2] - queryPoints[1, queryKeypointIndex1],
                                        queryPoints[0, queryKeypointIndex2] - queryPoints[0, queryKeypointIndex1])

                # angle between point 1 and 2, within train image
                trainAngle = math.atan2(trainPoints[1, trainKeypointIndex2] - trainPoints[1, trainKeypointIndex1],
                                        trainPoints[0, trainKeypointIndex2] - trainPoints[0, trainKeypointIndex1])

                # difference of angles
                diffAngle = queryAngle - trainAngle
                pipi = 2.0 * 3.1416;

                if abs(diffAngle) > pipi:
                    diffAngle = diffAngle / pipi
                    diffAngle = diffAngle - int(diffAngle)

                if diffAngle < 0.0:
                    diffAngle = diffAngle + pipi

                #rotates all key-points of train image
                sinAngle = math.sin(diffAngle)
                cosAngle = math.cos(diffAngle)

                rotationMatrix = numpy.zeros((2, 2))
                rotationMatrix[0, 0] = cosAngle
                rotationMatrix[0, 1] = -sinAngle
                rotationMatrix[1, 0] = sinAngle
                rotationMatrix[1, 1] = cosAngle
                trainPoints = numpy.mat(rotationMatrix) * numpy.mat(trainPoints)

                # translates all key-points of train image
                pointA1 = queryPoints[:, queryKeypointIndex1]
                pointA1.shape = (2, 1)
                pointB1 = trainPoints[:, trainKeypointIndex1]
                pointB1.shape = (2, 1)

                pointA2 = queryPoints[:, queryKeypointIndex2]
                pointA2.shape = (2, 1)
                pointB2 = trainPoints[:, trainKeypointIndex2]
                pointB2.shape = (2, 1)
                translationMatrix = (pointA1 - pointB1 + pointA2 - pointB2) / 2.0
                
                for k in range(trainImgKeypointCount):
                    trainPoints[:, k] = trainPoints[:, k] + translationMatrix

                # mounts the answer of the method, with only
                # the geometrically consistent matches
                consistentMatches = []
                for k in range(len(matches)):
                    xA = queryPoints[0, matches[k].queryIdx]
                    yA = queryPoints[1, matches[k].queryIdx]

                    xB = trainPoints[0, matches[k].trainIdx]
                    yB = trainPoints[1, matches[k].trainIdx]
                
                    if abs(xA - xB) < displMatchThreshold and abs(yA - yB) < displMatchThreshold:
                        consistentMatches.append(k)
                        usedMatches.append(k)
                        
                if len(consistentMatches) > len(ithMatches):
                    ithMatches = consistentMatches
                    
    if len(ithMatches) > 0:
        _filteredMatches.append(ithMatches)
        
def filterGeomConsMatches(queryImgKeypoints, trainImgKeypoints, matches,
                  refMatchCount = 50, displMatchThreshold = 20, threadCount = 24):
    global _filteredMatches
    
    matchCount = len(matches)
    if refMatchCount > matchCount:
        refMatchCount = matchCount
 
    for t in range(0, refMatchCount, threadCount):
        jobs = []
        
        for u in range(0, threadCount):
            if t + u < refMatchCount:
                job = multiprocessing.Process(target = _filterIthMatch,
                                              args = (queryImgKeypoints, trainImgKeypoints, matches, (t + u),
                                                      refMatchCount, displMatchThreshold, ))
                job.start()
                jobs.append(job)
        
        for job in jobs:
            job.join()
    
    matchIndexes = []
    for indexMatchSet in _filteredMatches:
        if len(indexMatchSet) > len(matchIndexes):
            matchIndexes = indexMatchSet
            
    answer = []
    for matchIndex in matchIndexes:
        answer.append(matches[matchIndex])
        
    del _filteredMatches[:]
    return answer

def drawMatches(img1, kp1, img2, kp2, matches, color = None):
    # https://gist.github.com/CannedYerins/11be0c50c4f78cad9549
    # @author: CannedYerins

    """Draws lines between matching keypoints of two images.  
    Keypoints not in a matching pair are not drawn.
    Places the images side by side in a new image and draws circles 
    around each keypoint, with line segments connecting matching pairs.
    You can tweak the r, thickness, and figsize values as needed.
    Args:
        img1: An openCV image ndarray in a grayscale or color format.
        kp1: A list of cv2.KeyPoint objects for img1.
        img2: An openCV image ndarray of the same format and with the same 
        element type as img1.
        kp2: A list of cv2.KeyPoint objects for img2.
        matches: A list of DMatch objects whose trainIdx attribute refers to 
        img1 keypoints and whose queryIdx attribute refers to img2 keypoints.
        color: The color of the circles and connecting lines drawn on the images.  
        A 3-tuple for color images, a scalar for grayscale images.  If None, these
        values are randomly generated.  
    """
    # We're drawing them side by side.  Get dimensions accordingly.
    # Handle both color and grayscale images.
    if len(img1.shape) == 3:
        new_shape = (max(img1.shape[0], img2.shape[0]), img1.shape[1]+img2.shape[1], img1.shape[2])
    elif len(img1.shape) == 2:
        new_shape = (max(img1.shape[0], img2.shape[0]), img1.shape[1]+img2.shape[1])
    new_img = numpy.zeros(new_shape, type(img1.flat[0]))  
    # Place images onto the new image.
    new_img[0:img1.shape[0],0:img1.shape[1]] = img1
    new_img[0:img2.shape[0],img1.shape[1]:img1.shape[1]+img2.shape[1]] = img2
    
    # Draw lines between matches.  Make sure to offset kp coords in second image appropriately.
    r = 20
    thickness = 4
    if color:
        c = color
    for m in matches:
        # Generate random color for RGB/BGR and grayscale images as needed.
        if not color: 
            c = numpy.random.randint(0,256,3) if len(img1.shape) == 3 else numpy.random.randint(0,256)
        # So the keypoint locs are stored as a tuple of floats.  cv2.line(), like most other things,
        # wants locs as a tuple of ints.        
        end1 = tuple(numpy.round(kp1[m.queryIdx].pt).astype(int))
        end2 = tuple(numpy.round(kp2[m.trainIdx].pt).astype(int) + numpy.array([img1.shape[1], 0]))
        cv2.line(new_img, end1, end2, c, thickness)
        cv2.circle(new_img, end1, r, c, thickness)
        cv2.circle(new_img, end2, r, c, thickness)
    
    return new_img

def warpMatches(keypoints1, image1, keypoints2, image2, matches):
    img1Points = []
    img2Points = []
    for match in matches:
        img1Points.append(keypoints1[match.queryIdx].pt)
        img2Points.append(keypoints2[match.trainIdx].pt)
    
    warps = []
    keypoints = []
    for i in range(0, 2):
        aPoints = []
        bPoints = []
        imageA = [[]]
        imageB = [[]]
        kps = []
        
        if i == 0:
            aPoints = img1Points
            bPoints = img2Points
            imageA = image1
            imageB = image2
            kps = _copyKeypoints(keypoints1)
        
        else:
            aPoints = img2Points
            bPoints = img1Points
            imageA = image2
            imageB = image1
            kps = _copyKeypoints(keypoints2)
        
        homography, _ = cv2.findHomography(numpy.array(aPoints), numpy.array(bPoints), cv2.LMEDS)
        
        warpedAPoints = cv2.perspectiveTransform(numpy.array([numpy.array(aPoints)]), homography)
        warpImage = cv2.warpPerspective(imageA, homography, (imageB.shape[1], imageB.shape[0]))
        
        warpedAPoints = warpedAPoints.reshape(len(aPoints), 2)
        if i == 0:
            j = 0
            for match in matches:
                kps[match.queryIdx].pt = (warpedAPoints[j][0], warpedAPoints[j][1])
                kps[match.queryIdx].size = keypoints2[match.trainIdx].size
                j = j + 1
        else:
            j = 0
            for match in matches:
                kps[match.trainIdx].pt = (warpedAPoints[j][0], warpedAPoints[j][1])
                kps[match.trainIdx].size = keypoints1[match.queryIdx].size
                j = j + 1
        
        warps.append(warpImage)
        keypoints.append(kps)
        
    return keypoints[0], warps[0], keypoints[1], warps[1], matches

def match(keypoints1, descriptions1, image1, keypoints2, descriptions2, image2,
          nndrThreshold = 0.8, refMatchCount = 50, displMatchThreshold = 20, threadCount = 24):
     # if there are more points in image 1 than in 2, swaps them
    swap = 0
    if len(keypoints1) < len(keypoints2):
        swap = 1
        keypoints1, keypoints2 = keypoints2, keypoints1
        descriptions1, descriptions2 = descriptions2, descriptions1
        image1, image2 = image2, image1
        
    # finds the good matches between the keypoints
    firstAndSecondMatches = []
    if len(descriptions1) > 0 and len(descriptions2) > 0:
        matcher = cv2.BFMatcher()
        firstAndSecondMatches = matcher.knnMatch(descriptions1, descriptions2, k = 2)
     
    goodMatches = []
    if len(firstAndSecondMatches) > 0:
        for i, (a, b) in enumerate(firstAndSecondMatches):
            if b.distance != 0 and a.distance / b.distance < nndrThreshold:
                goodMatches.append(a)
        
        goodMatches = filterGeomConsMatches(keypoints1, keypoints2, goodMatches, refMatchCount, displMatchThreshold, threadCount)
    
    if(len(goodMatches) > 4):
        # warps the images
        warpedKps1, warpedImg1, warpedKps2, warpedImg2, goodMatches = warpMatches(keypoints1, image1, keypoints2, image2, goodMatches)
    
        # draws the matches
        matchImage1 = drawMatches(warpedImg1, warpedKps1, image2, keypoints2, goodMatches)
        matchImage2 = drawMatches(image1, keypoints1, warpedImg2, warpedKps2, goodMatches)
    
        # re-swaps the matches, if it is the case
        if swap == 1:
            keypoints1, keypoints2 = keypoints2, keypoints1
            descriptions1, descriptions2 = descriptions2, descriptions1
            image1, image2 = image2, image1
            
            warpedKps1, warpedKps2 = warpedKps2, warpedKps1
            warpedImg1, warpedImg2 = warpedImg2, warpedImg1
        
            for match in goodMatches:
                match.queryIdx, match.trainIdx = match.trainIdx, match.queryIdx
            
        return warpedKps1, warpedImg1, warpedKps2, warpedImg2, goodMatches, matchImage1, matchImage2
    
    else:
        # re-swaps the matches, if it is the case
        if swap == 1:
            keypoints1, keypoints2 = keypoints2, keypoints1
            descriptions1, descriptions2 = descriptions2, descriptions1
            image1, image2 = image2, image1
            
            for match in goodMatches:
                match.queryIdx, match.trainIdx = match.trainIdx, match.queryIdx
            
        matchImage = drawMatches(image1, keypoints1, image2, keypoints2, goodMatches)
        return keypoints1, image1, keypoints2, image2, goodMatches, matchImage, matchImage
