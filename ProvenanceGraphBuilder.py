# builds two graphs based on (i) keypoint count and (ii) average distance
import numpy, cv2, ImageDescriptor, KeypointMatcher

class Vertex(object):
    
    def __init__(self, id):
        self.id = id
        self.leader = id
        self.set = [self]
        
    def join(self, v):
        newLeader = self.leader
        newSet = self.set + v.set
        
        for _v in newSet:
            _v.leader = newLeader            
            del _v.set
            _v.set = newSet
    
def applyKruskal(adjMatrix, maxST = True):
    vertexCount = adjMatrix.shape[0]
    answer = numpy.zeros((vertexCount, vertexCount))
    
    vertices = []
    edges = []
    for i in range(vertexCount):
        vertices.append(Vertex(i))
        
        for j in range(vertexCount):
            edges.append((i, j, adjMatrix[i, j]))
    
    edges.sort(key=lambda e: e[2], reverse = maxST)
    
    currentEdge = 0
    while vertexCount > 1:
        i = edges[currentEdge][0]
        j = edges[currentEdge][1]
    
        if vertices[i].leader != vertices[j].leader:
            answer[i, j] = 1.0
            answer[j, i] = 1.0
            vertices[i].join(vertices[j])
            vertexCount = vertexCount - 1
        
        currentEdge = currentEdge + 1
    
    return answer

def buildGraph(imageListFilePath, outputFilePath):
    imageFilePaths = []
    descriptionFilePaths = []    
    
    file = open(imageListFilePath, 'r')
    for filePath in file:
        imageFilePaths.append(filePath.strip())
    file.close()
    
    # adjancency matrices
    imageCount = len(imageFilePaths)
    kpCountAdjMatrix = numpy.zeros((imageCount, imageCount))
    avgDistAdjMatrix = numpy.zeros((imageCount, imageCount)) * -1.0
    
    # image descriptions and keypoints
    keypoints = []
    descriptions = []
    for imageFilePath in imageFilePaths:
        img = cv2.imread(imageFilePath)
        if img is not None:
            kps, descs, _ = ImageDescriptor.surfDescribe(img)
            keypoints.append(kps)
            descriptions.append(descs)
        else:
            keypoints.append[[]]
            descriptions.append[[]]
    
    # for each pair of images
    for i in range(imageCount):
        imageI = cv2.imread(imageFilePaths[i])
        if imageI is not None:
            for j in range(i + 1, imageCount):
                print "Processing", i + 1, j + 1, imageCount
                
                distances = []
                imageJ = cv2.imread(imageFilePaths[j])
                if imageJ is not None:
                    _, _, _, _, matches, _, _ = KeypointMatcher.match(keypoints[i], descriptions[i], imageI,
                                                                      keypoints[j], descriptions[j], imageJ)
                    
                    for match in matches:
                        distances.append(match.distance)
                    
                    if len(distances) > 0:
                        kpCountAdjMatrix[i, j] = kpCountAdjMatrix[j, i] = len(distances)
                        avgDistAdjMatrix[i, j] = avgDistAdjMatrix[j, i] = numpy.average(distances)
    
    # applies Kruskal on both matrices
    kpCountTree = applyKruskal(kpCountAdjMatrix, maxST = True)
    avgDistTree = applyKruskal(avgDistAdjMatrix, maxST = False)
    numpy.savez(outputFilePath, kpCountAdjMatrix, avgDistAdjMatrix, kpCountTree, avgDistTree)
